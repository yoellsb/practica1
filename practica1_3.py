# main
if __name__ == "__main__":
    lista_letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K']
    lista_posicion = []
    for elemento in lista_letras:
        lista_posicion.append(elemento + ':' + str(lista_letras.index(elemento) + 1) + '\n')
    for elemento in lista_posicion:
        print(elemento)

    with open('salida-ej3.txt', 'w') as descriptor_fichero:
        descriptor_fichero.writelines(lista_posicion)